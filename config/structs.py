class File:

    def __init__(self, checksum, filename, size, pid, download_url):
        self.checksum = checksum
        self.filename = filename
        self.size = size
        self.pid = pid
        self.download_url = download_url


class Dataset:

    def __init__(self, master_id, version, data_node, replica):
        self.master_id = master_id
        self.version = version
        self.data_node = data_node
        self.replica = replica
        self.pid = None
        self.citation = None
        self.files = set()

    def set_pid(self, pid):
        self.pid = pid

    def set_citation(self, citation):
        self.citation = citation

    def add_file(self, f):
        self.files.add(f)
