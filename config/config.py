import sys
import sqlite3

from datetime import datetime
from ConfigParser import ConfigParser


class Config:

    def __init__(self, config_file):
        config = ConfigParser()
        config.read(config_file)

        self.config = config
        self.index_node = self.get_config('INDEX_NODE')
        self.handle_url = self.get_config('HANDLE_URL')
        self.handle_api = self.get_config('HANDLE_API')
        self.db = self.get_config('DB_LOCATION')
        self.loglevel = self.get_config('LOGLEVEL')

    def get_config(self, param):
        try:
            return self.config.get('DEFAULT', param)
        except:
            print 'Missing config option: %s' %param
            sys.exit(0)


class DBCon:

    def __init__(self, db_path):
        self.db_path = db_path
        self.conn = sqlite3.connect(self.db_path)
        self.c = self.conn.cursor()

        # create table for datasets and files, if not exists
        self.c.execute('''
                    CREATE TABLE IF NOT EXISTS datasets(
                        id INTEGER PRIMARY KEY,
                        name VARCHAR(256) NOT NULL,
                        version INTEGER NOT NULL,
                        pid VARCHAR(256) NOT NULL,
                        citation VARCHAR(256))
                    ''')

        # create table for datasets and files, if not exists
        self.c.execute('''
                    CREATE TABLE IF NOT EXISTS replica_datasets(
                        id INTEGER PRIMARY KEY,
                        name VARCHAR(256) NOT NULL,
                        version INTEGER NOT NULL,
                        pid VARCHAR(256) NOT NULL,
                        data_node VARCHAR(256) NOT NULL,
                        citation VARCHAR(256))
                    ''')
        self.commit()

    def commit(self):
        self.conn.commit()

    def add_dataset(self, name, version, pid, citation):
        self.c.execute('INSERT INTO datasets(name, version, pid, citation) VALUES (?,?,?,?)', [name, version, pid, citation])
        self.commit()

    def add_replica(self, name, version, pid, data_node, citation):
        self.c.execute('INSERT INTO replica_datasets(name, version, pid, data_node, citation) VALUES (?,?,?,?,?)',
                       [name, version, pid, data_node, citation])
        self.commit()

    def dataset_exists(self, pid):
        self.c.execute('SELECT id FROM datasets WHERE pid=?', [pid])
        if self.c.fetchone():
            return True
        else:
            return False

    def replica_exists(self, pid, data_node):
        self.c.execute('SELECT id FROM replica_datasets WHERE pid=? and data_node=?', [pid, data_node])
        if self.c.fetchone():
            return True
        else:
            return False

    def close(self):
        self.conn.close()


class Log:

    def __init__(self, logfile_path, level):
        self.logfile_path = logfile_path
        self.logfile = self.init_logging()
        self.level = level.upper()

    def init_logging(self):
        return open(self.logfile_path, 'a')

    def close(self):
        self.logfile.close()

    def warning(self, message):
        datetime_now = str(datetime.now()).split('.')[0]
        msg = '%s - WARNING - %s\n' % (datetime_now, message)
        self.logfile.write(msg)
        # print(msg)

    def debug(self, message):
        if self.level in ['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL']:
            datetime_now = str(datetime.now()).split('.')[0]
            msg = '%s - DEBUG - %s\n' % (datetime_now, message)
            self.logfile.write(msg)
            # print(msg)

    def info(self, message):
        if self.level in ['INFO', 'WARNING', 'ERROR', 'CRITICAL']:
            datetime_now = str(datetime.now()).split('.')[0]
            msg = '%s - INFO - %s\n' % (datetime_now, message)
            self.logfile.write(msg)
            # print(msg)
