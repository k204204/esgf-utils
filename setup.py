# !/usr/bin/env python

import os
import shutil

from setuptools import setup, find_packages

VERSION = 0.0

conf_final_location = '/esg/config/pid_citation_check.conf'
if not os.path.exists(conf_final_location):
    shutil.copyfile('./config.cfg', conf_final_location)

setup(
    name='pid_citation_check',
    version=VERSION,
    description='Check PID and Citation of already published data.',
    author='Katharina Berger',
    author_email='berger@dkrz.de',
    install_requires=[
        "esgf-pyclient>=0.1.8",
    ],
    packages=find_packages(),
    data_files=[
        ('conf_files', ['config.cfg'])
    ],
    include_package_data=True,
    scripts=[
        'check_publication',
    ],
    zip_safe=False,  # Migration repository must be a directory
)

