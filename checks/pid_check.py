import json
import requests

from config import File
from utils import FilePID, DatasetPID
from pyesgf.search import SearchConnection


def check_pid(pid, id, config, log, check_pid_content):
    handle = pid[4:]
    response = requests.get(config.handle_url + config.handle_api + handle)
    if not response.status_code == 200:
        log.warning('Cannot resolve PID: %s (%s)' % (id, pid))
        return False, None
    else:
        if check_pid_content:
            return True, response
    return True, None


def pid_check_dataset(dataset, config, log, check_pid_content, search_params):
    dataset_id_version = '%s.v%s' % (dataset.master_id, dataset.version)
    dataset_pid_valid, response = check_pid(dataset.pid, dataset_id_version, config, log, check_pid_content)
    # check children only in case dataset PID is present
    if dataset_pid_valid:
        search_params['pid'] = dataset.pid
        children_pids_valid = pid_check_children(dataset, search_params, config, log, check_pid_content)
        if children_pids_valid:
            log.info('Dataset and File PIDs found for %s.' % dataset_id_version)
        if check_pid_content:
            dataset_pid_valid = check_dataset_pid_metadata(response, dataset, log)
        return dataset_pid_valid and children_pids_valid
    return False


def pid_check_children(dataset, search_params, config, log, check_pid_content):
    children_pids_valid = True
    conn = SearchConnection('http://%s/esg-search' % config.index_node)
    ctx = conn.new_context(**search_params)
    files = ctx.search()[0].file_context().search()
    for f in files:
        fs = File(f.checksum, f.filename, f.size, f.tracking_id, f.download_url)
        dataset.add_file(fs)
        current_child_valid, response = check_pid(f.tracking_id, f.filename, config, log, check_pid_content)
        children_pids_valid = children_pids_valid and current_child_valid
        if current_child_valid and check_pid_content:
            success = check_file_pid_metadata(response, fs, dataset.pid, dataset.replica, log)
            children_pids_valid = children_pids_valid and success
    return children_pids_valid


def check_dataset_pid_metadata(response, dataset, log):
    success = True
    dataset_pid = DatasetPID(response)
    if dataset_pid.metadata_complete:
        if dataset_pid.aggregation_level != 'DATASET':
            log.warning('Dataset - Aggregation_level mismatch: %s' % dataset.pid)
            success = False
        if dataset_pid.master_id != dataset.master_id:
            log.warning('Dataset - drs_id mismatch: %s' % dataset.pid)
            success = False
        if dataset_pid.version != dataset.version:
            log.warning('Dataset - version mismatch: %s' % dataset.pid)
            success = False
        if not dataset.replica:
            if dataset.data_node not in dataset_pid.data_nodes:
                log.warning('Dataset - data_node mismatch: %s' % dataset.master_id)
                success = False
        else:
            if dataset.data_node not in dataset_pid.data_nodes_replica:
                log.warning('Dataset - data_node mismatch: %s' % dataset.master_id)
                success = False
        file_pids = set(f.pid for f in dataset.files)
        if set(dataset_pid.file_pids) != set(file_pids):
            log.warning('Dataset - Children mismatch: %s' % dataset.master_id)
            success = False
    else:
        log.warning('Dataset - Missing metadata: %s' % dataset.master_id)
        success = False

    return success


def check_file_pid_metadata(response, file_searchapi, dataset_pid, replica, log):
    success = True
    file_pid = FilePID(response)
    if file_pid.metadata_complete:
        if file_pid.aggregation_level != 'FILE':
            log.warning('File - Aggregation_level mismatch: %s' % file_searchapi.pid)
            success = False
        if file_pid.size != str(file_searchapi.size):
            log.warning('File - Size mismatch: %s' % file_searchapi.pid)
            success = False
        if file_pid.checksum != str(file_searchapi.checksum):
            log.warning('File - Checksum mismatch: %s' % file_searchapi.pid)
            success = False
        if file_pid.filename != str(file_searchapi.filename):
            log.warning('File - Filename mismatch: %s' % file_searchapi.pid)
            success = False
        if not replica:
            if str(file_searchapi.download_url) not in file_pid.download_url:
                print file_searchapi.download_url, file_pid.download_url
                log.warning('File - URL mismatch: %s' % file_searchapi.pid)
                success = False
        else:
            if str(file_searchapi.download_url) not in file_pid.download_urls_replica:
                print file_searchapi.download_url, file_pid.download_urls_replica
                log.warning('File - URL mismatch: %s' % file_searchapi.pid)
                success = False
        if dataset_pid not in file_pid.dataset_pids:
            log.warning('File - Dataset mismatch: %s' % file_searchapi.pid)
            success = False
    else:
        log.warning('File - Missing metadata: %s' % file_searchapi.filename)
        success = False

    return success
