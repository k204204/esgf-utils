import json
import requests


def check_citation(citation_url, log):
    response = requests.get(citation_url)
    if not response.status_code == 200:
        log.warning('Citation not found: %s' % citation_url)
        return False
    else:
        parts = response.content.split(',')
        for p in parts:
            if 'creators' in p:
                creators = p.split(':')[-1].strip()
                if len(creators) < 5:
                    return False
                break
    return True
