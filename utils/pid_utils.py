import json


def _lookup_metadata(pid_response, lookup_field):
    try:
        pid_values = json.loads(pid_response.content)['values']
        return (item['data']['value'] for item in pid_values if item['type'] == lookup_field).next()
    except:
        return None


class FilePID:

    def __init__(self, pid_response):
        self.pid_response = pid_response
        self.aggregation_level = str(_lookup_metadata(self.pid_response, 'AGGREGATION_LEVEL'))
        self.size = str(_lookup_metadata(self.pid_response, 'FILE_SIZE'))
        self.checksum = str(_lookup_metadata(self.pid_response, 'CHECKSUM'))
        self.filename = str(_lookup_metadata(self.pid_response, 'FILE_NAME'))
        try:
            self.download_url = str(_lookup_metadata(self.pid_response, 'URL_ORIGINAL_DATA')).split('href="')[1].split('" /></locations>')[0].split('"')[0]
        except:
            self.download_url = None
        try:
            replica_urls = str(_lookup_metadata(self.pid_response, 'URL_REPLICA')).split('" /></locations>')[0].split('href="')
            self.download_urls_replica = set()
            for url in replica_urls:
                self.download_urls_replica.add(url.split('"')[0])
        except:
            self.download_urls_replica = None
        self.dataset_pids = str(_lookup_metadata(self.pid_response, 'IS_PART_OF')).split(';')
        # False iff any is None or empty
        dn_present = any([self.download_url, self.download_urls_replica])
        self.metadata_complete = all([self.aggregation_level, self.size, self.checksum, self.filename, dn_present, self.dataset_pids])


class DatasetPID:

    def __init__(self, pid_response):
        self.pid_response = pid_response
        self.aggregation_level = str(_lookup_metadata(self.pid_response, 'AGGREGATION_LEVEL'))
        self.master_id = str(_lookup_metadata(self.pid_response, 'DRS_ID'))
        self.version = str(_lookup_metadata(self.pid_response, 'VERSION_NUMBER'))
        try:
            self.data_nodes = str(_lookup_metadata(self.pid_response, 'HOSTING_NODE')).split('" /></locations>')[0].split('host="')[1].split('"')
        except:
            self.data_nodes = None
        try:
            self.data_nodes_replica = str(_lookup_metadata(self.pid_response, 'REPLICA_NODE')).split('" /></locations>')[0].split('host="')[1].split('"')
        except:
            self.data_nodes_replica = None
        self.file_pids = str(_lookup_metadata(self.pid_response, 'HAS_PARTS')).split(';')
        # False iff any is None or empty
        dn_present = any([self.data_nodes, self.data_nodes_replica])
        self.metadata_complete = all([self.aggregation_level, self.master_id, self.version, dn_present, self.file_pids])
